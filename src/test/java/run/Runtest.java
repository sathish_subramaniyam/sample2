package run;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.testng.AbstractTestNGCucumberTests;
@CucumberOptions(features="src/test/java/cucum/data.feature", glue ="steps" , monochrome = true,dryRun = true , snippets = SnippetType.CAMELCASE)
public class Runtest extends AbstractTestNGCucumberTests{

}
