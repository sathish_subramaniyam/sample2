package Pages;

import java.util.ArrayList;

import common.Annotation;
import cucumber.api.java.en.Given;

public class Register extends Annotation {

	@Given("Pass passport number")
	public Register reg()
	{
     driver.findElementByXPath("(//input[@id='MainContentFull_RegisterControl_StepControl_CoreClient_txtIDNumber_txField'])[1]").sendKeys("pass");
     return this;
	}
	
	@Given("Pass Mobile number")
	public Register mob()
	{
		driver.findElementByXPath("(//span[text()='Mobile number']/following::input)[1]").sendKeys("42342342");
	    return this;
	}
	
	
	
	
	
}
