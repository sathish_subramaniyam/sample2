package Pages;

import java.util.ArrayList;

import common.Annotation;
import cucumber.api.java.en.Given;

public class Home extends Annotation {

	@Given("Click register button")
	public Register homescreen()
	{
		driver.findElementByXPath("(//a[@href='https://ib.africanbank.co.za/modules/Registration/Public/Register.aspx'])[2]").click();
        ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());	
        driver.switchTo().window(tab.get(1));
        return new Register();
	
	}
	
	
	
	
	
}
